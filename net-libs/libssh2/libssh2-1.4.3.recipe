SUMMARY="A SSH2 library."
DESCRIPTION="
libssh2 is a library implementing the SSH2 protocol, available under \
the revised BSD license.
"
HOMEPAGE="http://www.libssh2.org"
COPYRIGHT="
	2004-2007 Sara Golemon <sarag@libssh2.org>
	2005,2006 Mikhail Gusarov <dottedmag@dottedmag.net>
	2006-2007 The Written Word, Inc.
	2007 Eli Fant <elifantu@mail.ru>
	2009 Daniel Stenberg
	2008, 2009 Simon Josefsson
	"
LICENSE="BSD (3-clause)"
SRC_URI="http://www.libssh2.org/download/libssh2-$portVersion.tar.gz"
CHECKSUM_SHA256="eac6f85f9df9db2e6386906a6227eb2cd7b3245739561cad7d6dc1d5d021b96d"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PATCHES="libssh2-$portVersion.patchset"

PROVIDES="
	libssh2$secondaryArchSuffix = $portVersion compat >= 1.4
	lib:libssh2$secondaryArchSuffix = 1.0.1 compat >= 1
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	lib:libssl$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libcrypto$secondaryArchSuffix
	devel:libssl$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	autoreconf -I m4 -f -i
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libssh2
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir \
		$manDir/man3
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libssh2${secondaryArchSuffix}_devel = $portVersion compat >= 1.4
	devel:libssh2$secondaryArchSuffix = 1.0.1 compat >= 1
	"
REQUIRES_devel="
	libssh2$secondaryArchSuffix == $portVersion base
	"
